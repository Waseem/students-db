<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StudentsController extends Controller
{
	/**
	 * @Cache(expires="+15 minutes")
	 * @Route("/students/detail/{slug}", name="student_details")
	 * @Template()
	 */
	public function detailAction($slug)
	{
		/** @var \AppBundle\Repository\StudentRepository $repository */
		$repository = $this->getDoctrine()->getRepository('AppBundle:Student');

		/** @var \AppBundle\Entity\Student $record */
		$record = $repository->findOneBy(['path' => $slug]);

		if ($record === null) {
			throw $this->createNotFoundException('Student with given path not found');
		}

		return [
			'name' => $record->getName(),
			'description' => $record->getDescription(),
			'timestamp' => date('Y-m-d H:i:s')
		];
	}
}