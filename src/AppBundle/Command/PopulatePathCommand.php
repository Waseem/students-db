<?php

namespace AppBundle\Command;

use AppBundle\Service\StudentService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PopulatePathCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('app:student:path')
			->setDescription('Populates Path for Students')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		/** @var StudentService $service */
		$service = $this->getContainer()->get('app.student');

		$profiledOutput = $this->profile(array($service, 'populatePath'));
		$duplicates = var_export(array_filter($profiledOutput['returned']), true);

		$output->writeln('Output:');
		$output->writeln("\tDuplicates: " . $duplicates);

		$this->renderProfiledData($output, $profiledOutput);
	}

	protected function profile(callable $who)
	{
		// Bookmarking current time to compute diff later
		$startTime = microtime(true);

		$returnValue = call_user_func($who);

		// Compute profiling data
		$execTime = round(microtime(true) - $startTime, 2);
		$memUsage = memory_get_usage();

		return [
			'returned' => $returnValue,
			'profile' => compact('memUsage', 'execTime')
		];
	}

	protected function renderProfiledData(OutputInterface $output, array $profileData)
	{
		$memUsage = round($profileData['profile']['memUsage'] / (1024 * 1024), 2);

		$output->writeln(sprintf("\tMemory usage: %sMB", $memUsage));
		$output->writeln(sprintf("\tTime: %ds", $profileData['profile']['execTime']));
	}
}
