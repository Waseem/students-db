<?php

namespace AppBundle\Service;

use AppBundle\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;

class StudentService
{
	const BATCH_SIZE = 500;

	/**
	 * @var \Doctrine\ORM\EntityManagerInterface
	 */
	private $em;

	/**
	 * Student Service constructor
	 *
	 * @param EntityManagerInterface $entityManager
	 */
	public function __construct(EntityManagerInterface $entityManager)
	{
		$this->em = $entityManager;

		// Prevent logger to slow us down
		$this->em->getConfiguration()->setSQLLogger(null);
	}

	public function populatePath()
	{
		$batchCounter = 1;
		$dictionary = new \ArrayObject();
		$qb = $this->em->createQueryBuilder();

		$resultSet = $qb
			->select('s')
			->from('AppBundle:Student', 's')
			->getQuery()
			->iterate();

		while (($record = $resultSet->next()) !== false) {
			/** @var Student $record */
			$record = array_pop($record);

			$path = preg_replace('~\W~', '_', strtolower($record->getName()));

			if ($dictionary->offsetExists($path)) {
				$suffix = $dictionary->offsetGet($path);
				$dictionary->offsetSet($path, ++$suffix);
				$path .= '_' . $suffix;
			} else {
				$dictionary->offsetSet($path, 0);
			}

			$record->setPath($path);
			$this->em->persist($record);

			if ($batchCounter % self::BATCH_SIZE == 0) {
				$this->em->flush();
				$this->em->clear();
				gc_collect_cycles();
			}

			$batchCounter++;
		}

		return $dictionary->getArrayCopy();
	}
}