<?php

namespace tests\AppBundle\Service;

use AppBundle\Service\StudentService;

class StudentServiceTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * Tests for unique path population for student records
	 *
	 * @covers StudentService::populatePath
	 */
	public function testPopulatePath()
	{
		// 1, Generate test data with expected results
		$testData = $this->buildTestData();

		// 2, Generate entity manager mock which will provide mocked data
		$emMock = $this->createEntityManagerMock($testData);

		// 3, Initialize test target
		$target = new StudentService($emMock);

		// 4, Run the test!
		$target->populatePath();
	}

	/**
	 * Creates Entity-Manager mock
	 *
	 * The mock will support:
	 * - Disabling SQL Logger,
	 * - Get mock of query-builder to return iterate-able result for given mock objects
	 * - At least one call to Persist,
	 * - Any number of calls to Flush & Clear
	 *
	 * @param array $queryResult
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function createEntityManagerMock(array $queryResult)
	{
		// 1, Config object mock
		$configMock = $this->getMockBuilder('Doctrine\ORM\Configuration')
			->disableOriginalConstructor()
			->getMock();

		$configMock->expects($this->once())
			->method('setSQLLogger')
			->with($this->isNull());

		// 2, Iterable Result
		// Since Doctrine\ORM\Internal\Hydration\IterableResult class is not pure \Iterator implementation, we need to mock it too :/
		$iteratorMock = $this->getMockBuilder('Doctrine\ORM\Internal\Hydration\IterableResult')
			->disableOriginalConstructor()
			->getMock();

		// Adding iterator termination condition
		$queryResult[] = false;

		// mocking iterator
		$iteratorMock->method('next')
			->willReturnOnConsecutiveCalls(...$queryResult);

		// 3, Query object mock (cannot use Query / NativeQuery because its declared as Final!)
		$queryMock = $this->getMockBuilder('Doctrine\ORM\AbstractQuery')
			->disableOriginalConstructor()
			->getMock();

		$queryMock->expects($this->once())
			->method('iterate')
			->willReturn($iteratorMock);

		// 4, QueryBuilder object mock
		$queryBuilderMock = $this->getMockBuilder('Doctrine\ORM\QueryBuilder')
			->disableOriginalConstructor()
			->getMock();

		// 4a, QueryBuilder->select
		$queryBuilderMock->expects($this->once())
			->method('select')
			->withAnyParameters()
			->willReturnSelf();

		// 4b, QueryBuilder->from
		$queryBuilderMock->expects($this->once())
			->method('from')
			->withAnyParameters()
			->willReturnSelf();

		// 4c, QueryBuilder->getQuery
		$queryBuilderMock->expects($this->once())
			->method('getQuery')
			->willReturn($queryMock);

		// Wiring up all together
		// Entity manager mock
		$mock = $this->getMockBuilder('Doctrine\ORM\EntityManager')
			->disableOriginalConstructor()
			->getMock();

		// - getConfiguration
		$mock->expects($this->once())
			->method('getConfiguration')
			->willReturn($configMock);

		// - createQueryBuilder
		$mock->expects($this->once())
			->method('createQueryBuilder')
			->willReturn($queryBuilderMock);

		// - persist
		$mock->expects($this->atLeastOnce())
			->method('persist')
			->withAnyParameters();

		// - flush
		$mock->expects($this->any())
			->method('flush');

		// - clear
		$mock->expects($this->any())
			->method('clear');

		return $mock;
	}

	/**
	 * Builds test data with output expectations
	 *
	 * @return array
	 */
	private function buildTestData()
	{
		return [
			[ $this->createEntityMock('Daniel Catz', 'daniel_catz') ],
			[ $this->createEntityMock('Maksym Moskvychev', 'maksym_moskvychev') ],
			[ $this->createEntityMock('Waseem Ahmed', 'waseem_ahmed') ],
			[ $this->createEntityMock('Waseem Ahmed', 'waseem_ahmed_1') ],
		];
	}

	/**
	 * Creates entity mock object with sought input and desired output
	 *
	 * @param string $inputName
	 * @param string $outputPath
	 *
	 * @return \PHPUnit_Framework_MockObject_MockObject
	 */
	private function createEntityMock($inputName, $outputPath)
	{
		// Entity object mock
		$entityMock = $this->getMockBuilder('AppBundle\Entity\Student')
			->getMock();

		$entityMock->expects($this->once())
			->method('getName')
			->willReturn($inputName);

		$entityMock->expects($this->once())
			->method('setPath')
			->with($outputPath);

		return $entityMock;
	}
}
